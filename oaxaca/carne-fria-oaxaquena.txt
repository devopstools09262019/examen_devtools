Receta mexicana de Carne fría oaxaqueña del Estado de Oaxaca, México.

1 kilo metlapil de res (cuete)
50 g almendras
50 g jamón
50 g tocino
1/2 litro de agua
1/2 taza de vinagre
8 ciruelas pasas
4 chiles anchos
4 hojas de laurel
1 cucharadita de azúcar
1 cucharadita de orégano
1 lechuga
1 manojo de rabanitos
1 rajita de canela
hierbas de olor
pimienta
sal
Para preparar carne fría oaxaqueña hay que mechar el metlapil con tocino, jamón, almendras y ciruelas pasas; espolvorearlo con sal y pimienta. Dorar en aceite caliente; agregarle el vinagre mezclado con agua, hierbas de olor, orégano, los chiles anchos asados y desvenados, canela, laurel y azúcar. Tapar el recipiente y dejar a fuego suave hasta que el guiso se cueza; dejarlo enfriar. Rebanar la carne, acomodarla en un platón y adornar con hojas de lechuga y rabanitos abiertos. La receta alcanza para 8 raciones.


