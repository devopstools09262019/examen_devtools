Esta receta mexicana de Amarillo con carne de res de Oaxaca, México lleva los siguientes ingredientes:

1 kilo carne de res
250 g papas
250 g ejotes
100 g chile chilcoxle o guajillo
50 g masa de maíz
4 dientes de ajo
4 tomates verdes
4 pimientas
3 jitomates
2 clavos
2 chayotes
2 hojas de hierbasanta
1 cebolla chica
1 pizca de cominos
manteca
sal
Para preparar amarillo con carne de res hay que cocer la carne en agua suficiente, con ajo y sal. Desvenar y tostar el chile y remojarlo y molerlo con las especias. Freír en manteca y agregar el caldo de la carne, los ejotes y chayo tes (previamente cocidos y rebanados), las papas cocidas, hojas de hierbasanta, tomates, cebolla y jitomates picados. Sazonar con sal y dejar hervir. Desleír un poco de masa en caldo para espesar el guiso; al resto de la masa se le pone un poquito de manteca y sal. Formar bolitas con la masa, haciéndoles un hueco en el centro para que se cuezan con el guisado (se llaman chochoyotes). Servir con jugo de limón mezclado con cebolla finamente picada. La receta alcanza para 8 raciones.


