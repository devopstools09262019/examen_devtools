Receta mexicana de carne de puerco con frijoles de Chiapas, México.

1/2 kilo carne de puerco
1/4 kilo frijoles
5 pimientas negras
2 dientes de ajo
1 chile ancho
1 tomate
1 rama de epazote
chiles en vinagre
sal
Para preparar carne de puerco con frijoles hay que cocer la carne y los frijoles por separado. Licuar jitomates, ajo y pimientas con el chile ancho remojado. Revolver con la carne y el frijol cocido, dejar cocer quince minutos a fuego lento con los chiles en vinagre y epazote. Sal, al gusto. Esta receta alcanza para 8 raciones.


